// this require("express") allows devs to load/import express package that will be used for the application
let express = require("express");

// allows devs to create an application using express
const app = express(); //this code creates an express application and stores it inside the "app" variable, thus, "app" is the server already.

const port = 3000;

// use function lets the middleware to do common services and capabilities to the applications
app.use(express.json()); // lets the app to read json data
app.use(express.urlencoded({ extended: true })); //allows the app to receive data from the forms
// not using these will result to our req.body to return undefined

// Express has methods corresponding to each http methods (get, post, put, delete, etc)
// this "/" route expects to receive a get request at its endpoint(http://localhost:3000/)
app.get("/", (req,res)=>{
  // res.send - allows sending of messages as responses to the client
  res.send("Hello World")
})


/* 
miniactivity
  create a "/hello" route that will receive a GET request and will send a message to the client
    send the screenshot of your output in the batch gc
*/

app.get("/hello", (req, res) =>{
  res.send("hello from the /hello endpoint")
})

app.post("/hello", (req,res)=>{
  res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`)
})

app.get("/home", (req,res)=>{
  res.send("Welcome to the home page")
})


let users = [{
  "username": "janedoe",
  "password": "janedo12345"
}]
app.get("/users", (req,res)=>{
  res.send(users)
})

app.delete("/delete-user", (req,res)=>{
  res.send(`User ${req.body.username} has been deleted.`)
})

app.post("/signup", (req,res)=>{
  res.send(`User ${req.body.username} successfully registered`);
  users.push(req.body);
})

app.listen(port,() => console.log(`Server is running at port ${port}`))